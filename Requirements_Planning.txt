1.1 Requirements and Planning

        User Stories
        1. As a Scientist I want to be able to collect data remotely and in person so that I am able to study the results.

        2. As a Software Engineer I want to create a software which is easy to use and maintain so that all individuals within the base will be able to use and access areas related to them.

        3. As an Electrical Engineer I want to be able to locate circuits which there may be issues so that issues can be fixed before developing further.

        4. As a Weather Monitor I want to be able to detect any serious weather conditions such as blizzards so that I am able to notify all individuals within the base to provide enough time to prepare for the conditions and safely ensure all equipment is stored correctly to prevent any damage.

        5. As an Ice Sheet Monitor I want to closely monitor the ice sheet for any slight changes in temperature or movement of the sheet so that dangers such as avalanches and fractures of the sheet itself

        6. As a Communications Officer I want to be able to contact the base using a communication system at all times so that any issues are assessed early on to prevent unforeseen circumstances arising and provide aid when needed.

        7. As a vehicle Operator I want to be able to track all the vehicles location within the base so that they can easily be located in emergencies.

        8. As a Structural Maintenance Officer I want to be able to be indicated as to where there is structural faults so that they can promptly and safely be fixed without using up a lot of time having to search for where there may be faults.

        9. As a Chef I want to be able to prepare food and inform individuals when it is ready to eat so that all members with in the base are provided with enough nutrition to survive in the cold climate conditions

        10. As a Mechanical Engineer I want to be able to fix experimental equipment from the base so that scientists can continuously record data and complete research without having to wait for the transportation of new equipment


        User Cases
        1-
                Goal:
                        To allow Weather Monitors to Detect Hazardous changes in weather

                Primary Actor:
                        Weather Monitor

                Secondary Actor:
                        Weather Sensors

                Preconditions:
                        The Weather Monitor must be log into the system and all the sensors are switched on and running correctly

                Trigger:
                        A change in the weather is detected by the Sensors
	Flow of Events:
                        1. The Sensors will indicate that there has been a change in the weather
                        2. The system will detect whether the change in weather is Hazardous
                        3. The system will display a warning sign to the monitor before issuing a signal to all other members of the base to secure all equipment
                        4. The system will begin to record all information associated with the change in weather in set intervals
                        5. The system will provide relevant information to the weather monitor as to which parts of the base will be affected and how much time until all people at the base must be in a safe and secure place
                        6. The system will continue to update the monitor with information
                        7. The system will detect when the weather begins to clear up and indicate when it is safe to exit the base again.

                Extensions:
                        2A - the change in weather is not Hazardous
                                1. The system will record the change in weather
                                2. The system will indicate that it is not a Hazardous change
                                3. The use case end

        2-
                Goal:
                        To allow Vehicle Operator to locate a specific Vehical within the base

                Primary Actor:
                        Vehicle Operator

                Secondary Actor:
                        Vehicle tracker

                Preconditions:
                        Vehicle operator must be logged into the software and all trackers on vehicles must be working

                Trigger:
                        Operator selects option to "Find Vehicle"

                Flow of Events:
                        1. The Operator will select the registered number of the vehicle which they wish to locate
                        2. The System will search for the location of the vehicle
                        3. The system will indicate if the vehicle is in use or stationery
                        4. The system will provide a map of the location of the vehicle
                        5. The system will calculate the quickest time it would take to access the vehicle with options of different paths
                        6. The system will print a map with the route selected

                Extensions:
                        1A - Incorrect vehicle is selected
                                1. The operator will select the option to return to the search page
                                2. The operator will select the registered number of the correct vehicle
                                3. The use case resumes at step 2
                        2A - Vehicle cannot be located
                                1. The system will indicate that the vehicle cannot be located and provide the last known location
                                5. The use case resumes at step 4


        3-
                Goal:
                        Allow Ice Sheet Monitors to detect drastic movement of the ice specific locations on the sheet

                Primary Actor:
                        Ice Sheet Monitor

                Secondary Actor:
                        Movement Sensors

                Preconditions:
                        Ice Sheet Monitor must be logged into the system and the sensors must be working and switched on

                Trigger:
                        Movement within the ice sheet

                Flow of Events:
                        1. The sensors will detect there has been a movement in the ice sheet
                        2. The system will calculate and record the distance the sheet has moved
                        3. The system will notify the Ice Sheet Monitor if the distance is greater than a set distance
                        4. The system will indicate which sensor the movement has been detected at
                        5. The system will continuously monitor the specific sensor closer to ensure no further movement occurs
                        6. The system will stop monitoring the sensor closely once the ice sheet monitor has indicated to do so.

                Extensions:
                        4A - Distance is extreme
                                1. The system will allow an option for the ice sheet monitor to send an alert to notify the base of the movement
                                2. The system will send an alert to the main office of the project in the outside world.
                                3. The system will allow the ice sheet monitor to indicate if the warning is severe or not
                                4. The use case resumes at step 5
